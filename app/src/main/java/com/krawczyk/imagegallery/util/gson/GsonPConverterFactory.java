package com.krawczyk.imagegallery.util.gson;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

/**
 * Gson converter factory, which uses {@link GsonPResponseBodyConverter}
 *
 * Created by Paweł Krawczyk on 14-01-2017.
 */
public final class GsonPConverterFactory extends Converter.Factory {

    private Gson mGson;

    public GsonPConverterFactory(Gson gson) {
        if (gson == null) throw new NullPointerException("mGson == null");
        this.mGson = gson;
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations,
                                                            Retrofit retrofit) {
        TypeAdapter<?> adapter = mGson.getAdapter(TypeToken.get(type));
        return new GsonPResponseBodyConverter<>(mGson, adapter);
    }

    @Override
    public Converter<?, RequestBody> requestBodyConverter(Type type,
                                                          Annotation[] parameterAnnotations,
                                                          Annotation[] methodAnnotations,
                                                          Retrofit retrofit) {
        return null;
    }
}