An Android appliation for downloading and displaying images from Flickr's public photos feed available [here](https://www.flickr.com/services/feeds/docs/photos_public).

### Available functionalities ###
1. Shows Flickr's public photos feed by default, after starting the app.
2. Search for public images connected with given tag using search bar on the top. 
3. View full image metadata by pressing on the corresponding thumbnail.
4. Send media via email.
5. View media in the internet browser.

### Other notes ### 
1. Images are cached using Picasso library.
2. Application works only when internet is available and contains network connectivity listener for showing animated "no internet connection" view, with possibility to enable WIFI in the settings.

### Used libraries###
1. Retrofit
2. Dagger 2
3. ButterKnife
4. DesertPlaceholder