package com.krawczyk.imagegallery.activities;

import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.hotellook.desertplaceholder.DesertPlaceholder;
import com.krawczyk.imagegallery.R;
import com.krawczyk.imagegallery.api.FlickrFeedDownloader;
import com.krawczyk.imagegallery.api.FlickrQueryCallback;
import com.krawczyk.imagegallery.ui.fragment.ImageGridFragment;
import com.krawczyk.imagegallery.util.NetworkUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.krawczyk.imagegallery.AppConfigurations.LANDSCAPE_SPAN;
import static com.krawczyk.imagegallery.AppConfigurations.PORTRAIT_SPAN;
import static com.krawczyk.imagegallery.R.id.fab;

/**
 * Entry activity of the applications. By default displays images from the Flickr's public photos
 * feed. Available features: 1) Search for public images connected with given tag using
 * search bar on the top. 2) View full image metadata by pressing on the corresponding
 * thumbnail.
 *
 * Created by Paweł Krawczyk on 14-01-2017.
 */
public class MainActivity extends AppCompatActivity
{
	@BindView(R.id.image_grid_container)
	LinearLayout mContentContainer;
	@BindView(fab)
	FloatingActionButton mFloatingActionButton;
	@BindView(R.id.placeholder)
	DesertPlaceholder mNoInternetPlaceholder;
	private ImageGridFragment mImageGridFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		initUiComponents();
		addImageGridFragment();
		downloadFeedAndUpdateImageGrid();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		registerReceiver(mNetworkChangeReceiver, new IntentFilter(
		        "android.net.conn.CONNECTIVITY_CHANGE"));
	}

	@Override
	public void onPause()
	{
		super.onPause();
		try
		{
			unregisterReceiver(mNetworkChangeReceiver);
		}
		catch (IllegalArgumentException e)
		{
			// In case receiver was already unregistered
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.main, menu);

		final MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
		// Responsible for handling search view
		SearchView searchView = (SearchView) myActionMenuItem.getActionView();
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
		{
			@Override
			public boolean onQueryTextSubmit(String query)
			{
				downloadFeedAndUpdateImageGridWithTag(query);
				myActionMenuItem.collapseActionView();
				return false;
			}

			@Override
			public boolean onQueryTextChange(String s)
			{
				// Do not search while typing to avoid huge network traffic
				return false;
			}
		});
		return true;
	}

	/**
	 * Initializes UI components
	 */
	private void initUiComponents()
	{
		ButterKnife.bind(this);
		mFloatingActionButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				Snackbar.make(view, R.string.hint_comment, Snackbar.LENGTH_LONG)
				        .setAction("Action", null).show();
			}
		});
		mNoInternetPlaceholder.setOnButtonClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
			}
		});
		updateNoNetworkUIState();
	}

	/**
	 * Add image grid fragment to the root layout
	 */
	private void addImageGridFragment()
	{
		FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
		mImageGridFragment = ImageGridFragment.newInstance(PORTRAIT_SPAN, LANDSCAPE_SPAN);
		fragTransaction.replace(mContentContainer.getId(), mImageGridFragment, "imageGridFragment");
		fragTransaction.commit();
	}

	/**
	 * Downloads the feed data from
	 * {@link com.krawczyk.imagegallery.api.ApiConstants#FLICKR_IMAGES_BASE_URL} and updated the
	 * {@link ImageGridFragment} with new data.
	 */
	private void downloadFeedAndUpdateImageGrid()
	{
		new FlickrFeedDownloader().getJsonData().enqueue(
		        new FlickrQueryCallback(mImageGridFragment));
	}

	/**
	 * Downloads the feed data corresponding to the given tag from
	 * {@link com.krawczyk.imagegallery.api.ApiConstants#FLICKR_IMAGES_BASE_URL} and updated the
	 * {@link ImageGridFragment} with new data.
	 */
	private void downloadFeedAndUpdateImageGridWithTag(String tag)
	{
		new FlickrFeedDownloader().getJsonData(tag).enqueue(
		        new FlickrQueryCallback(mImageGridFragment));
	}

	/**
	 * Updates the state of UI components on the network connectivity state change.
	 */
	private void updateNoNetworkUIState()
	{
		mNoInternetPlaceholder.setVisibility(!NetworkUtils.isNetworkAvailable() ? View.VISIBLE
		        : View.GONE);
		mFloatingActionButton.setVisibility(NetworkUtils.isNetworkAvailable() ? View.VISIBLE
		        : View.GONE);
	}

	/**
	 * BroadcastReceiver listening for the network connectivity changes. It is responsible for
	 * showing and hiding no network placeholder, when device changes connectivity state.
	 */
	private BroadcastReceiver mNetworkChangeReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateNoNetworkUIState();
		}
	};
}
