package com.krawczyk.imagegallery.api;

import com.krawczyk.imagegallery.ImageGallery;
import com.krawczyk.imagegallery.model.Feed;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * Feed downloader implementation for Flickr.
 * Downloads data from the general public photos feed specified in {@link ApiConstants}
 *
 * Created by Paweł Krawczyk on 14-01-2017.
 */
public class FlickrFeedDownloader implements IFeedDownloader
{
	@Inject
	Retrofit retrofit;

	private IFeedDownloader mImagesDownloader;

	public FlickrFeedDownloader()
	{
		ImageGallery.getInstance().getNetComponent().inject(this);
		mImagesDownloader = retrofit.create(IFeedDownloader.class);
	}

	/**
	 * Downloads Flickr's json data from the general public photos feed.
	 *
	 * @return Response with json data as a {@link Feed} object.
	 */
	@Override
	public Call<Feed> getJsonData()
	{
		return mImagesDownloader.getJsonData();
	}

	/**
	 * Downloads Flickr's json data from the general public photos feed, which are connected with
	 * the given tag.
	 *
	 * @param tag
	 *            Tag which represents the content category
	 * @return Response with json data as a {@link Feed} object.
	 */
	@Override
	public Call<Feed> getJsonData(String tag)
	{
		return mImagesDownloader.getJsonData(tag);
	}
}
