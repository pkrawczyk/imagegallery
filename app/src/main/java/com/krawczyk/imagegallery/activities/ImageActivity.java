package com.krawczyk.imagegallery.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.krawczyk.imagegallery.R;
import com.krawczyk.imagegallery.model.Items;
import com.krawczyk.imagegallery.util.IntentUtils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.krawczyk.imagegallery.R.id.fab_browser;
import static com.krawczyk.imagegallery.R.id.fab_email;

/**
 * An activity for displaying full screen image with metadata and provides actions for sending
 * emails and opening media in the internet browser.
 *
 * Created by Paweł Krawczyk on 14-01-2017.
 */
public class ImageActivity extends Activity
{
	private static final String ARG_ITEM = "item";

	@BindView(R.id.image)
	ImageView mImageView;
	@BindView(R.id.title)
	TextView mTitleTV;
	@BindView(R.id.author)
	TextView mAuthorTV;
	@BindView(R.id.author_id)
	TextView mAuthorIdTV;
	@BindView(R.id.date_taken)
	TextView mDateTakenTV;
	@BindView(R.id.published)
	TextView mPublishedTV;
	@BindView(R.id.link)
	TextView mLinkTV;
	@BindView(R.id.description)
	TextView mDescriptionTV;
	@BindView(R.id.tags)
	TextView mTagsTV;

	@BindView(fab_email)
	FloatingActionButton mFloatingActionEmail;
	@BindView(fab_browser)
	FloatingActionButton mFloatingActionBrowser;

	public static Intent getIntent(Context context, Items item)
	{
		Intent intent = new Intent(context, ImageActivity.class);
		intent.putExtra(ARG_ITEM, item);
		return intent;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image);
		ButterKnife.bind(this);
		Items item = getIntent().getParcelableExtra(ARG_ITEM);
		bindViewsWithMetadata(item);
		setFloatingActions();
		// Load image from cache
		Picasso.with(this).load(item.getMedia().getUrl())
		        .placeholder(R.drawable.ic_placeholder_download)
		        .error(R.drawable.ic_placeholder_error).into(mImageView);
	}

	private void bindViewsWithMetadata(Items item)
	{
		mTitleTV.setText(item.getTitle());
		mAuthorTV.setText(String.format(getString(R.string.author), item.getAuthor()));
		mAuthorIdTV.setText(String.format(getString(R.string.author_id), item.getAuthor_id()));
		mDateTakenTV.setText(String.format(getString(R.string.date_taken), item.getDate_taken()));
		mPublishedTV.setText(String.format(getString(R.string.published), item.getPublished()));
		mLinkTV.setText(String.format(getString(R.string.link), item.getLink()));
		mDescriptionTV
		        .setText(String.format(getString(R.string.description), item.getDescription()));
		mTagsTV.setText(String.format(getString(R.string.tags), item.getTags()));
	}

	private void setFloatingActions()
	{
		mFloatingActionEmail.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				Items item = getIntent().getParcelableExtra(ARG_ITEM);
				String body = "<a href=\"" + item.getLink() + "\">" + item.getLink() + "</a>";
				Intent emailIntent = new Intent(Intent.ACTION_SEND);
				emailIntent.putExtra(Intent.EXTRA_TEXT, "message");
				emailIntent.setType("message/rfc822");
				emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(body));
				boolean result = IntentUtils.tryToSendIntent(ImageActivity.this, emailIntent);
				Snackbar.make(view, result ? R.string.sending_email : R.string.no_application,
				        Snackbar.LENGTH_LONG).setAction("Action", null).show();
			}
		});
		mFloatingActionBrowser.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				Items item = getIntent().getParcelableExtra(ARG_ITEM);
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getLink()));
				boolean result = IntentUtils.tryToSendIntent(ImageActivity.this, browserIntent);
				Snackbar.make(view, result ? R.string.opening_in_browser : R.string.no_application,
				        Snackbar.LENGTH_LONG).setAction("Action", null).show();
			}
		});
	}
}
