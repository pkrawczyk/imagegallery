package com.krawczyk.imagegallery.ui.adapter;

import android.app.Application;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.krawczyk.imagegallery.ImageGallery;
import com.krawczyk.imagegallery.R;
import com.krawczyk.imagegallery.activities.ImageActivity;
import com.krawczyk.imagegallery.model.Items;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Adapter for downloading and displaying images from {@link com.krawczyk.imagegallery.model.Media} objects
 *
 * Created by Paweł Krawczyk on 14-01-2017.
 */
public class ImageAdapter extends RecyclerView.Adapter<ImageViewHolder> {

    @Inject
    Application mApplication;

    private final LayoutInflater mLayoutInflater;
    private List<Items> mItemsList = Collections.emptyList();

    public ImageAdapter() {
        ImageGallery.getInstance().getNetComponent().inject(this);
        this.mLayoutInflater = LayoutInflater.from(mApplication);
    }

    public void setMediaList(List<Items> itemsList) {
        this.mItemsList = itemsList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.image_grid_item, parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        final Items current = mItemsList.get(position);
        /**
         * Picasso uses a memory and disk cache to load previously requested image much faster.
         *
         * LRU memory cache of 15% the available application RAM
         * Disk cache of 2% storage space up to 50MB but no less than 5MB.
         * Three download threads for disk and network access.
         */
        Picasso.with(mApplication).load(current.getMedia().getUrl()).placeholder(R.drawable.ic_placeholder_download)
                .error(R.drawable.ic_placeholder_error).into(holder.getImageView());
        holder.getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = ImageActivity.getIntent(mApplication, current);
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                mApplication.startActivity(intent);
            }
        });
    }

    @Override
    public long getItemId(int i) {
        return RecyclerView.NO_ID;
    }

    @Override
    public int getItemCount() {
        return mItemsList.size();
    }
}
