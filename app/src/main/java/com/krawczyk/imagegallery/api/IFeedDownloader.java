package com.krawczyk.imagegallery.api;

import com.krawczyk.imagegallery.model.Feed;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.krawczyk.imagegallery.api.ApiConstants.PUBLIC_PHOTOS_URL_PART;

/**
 * Interface for feed downloader. Specifies available methods and the urls corresponding with given
 * API services.
 *
 * Created by Paweł Krawczyk on 14-01-2017.
 */
public interface IFeedDownloader
{
	@GET(PUBLIC_PHOTOS_URL_PART)
	Call<Feed> getJsonData();

	@GET(PUBLIC_PHOTOS_URL_PART)
	Call<Feed> getJsonData(@Query("tags") String tag);
}
