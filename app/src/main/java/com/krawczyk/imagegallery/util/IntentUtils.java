package com.krawczyk.imagegallery.util;

import android.content.Context;
import android.content.Intent;

/**
 * Utility class for sending intents.
 *
 * Created by Paweł Krawczyk on 14-01-2017.
 */
public class IntentUtils
{
	private IntentUtils(){
		//Do not initialize
	}
	/**
	 * Try to send intent using external application, but firstly verify if some application, which
	 * handles this intent exists. This check is being done in order to avoid
	 * {@link android.content.ActivityNotFoundException}
	 *
	 * @param context Current context
	 * @param sendIntent Intent to send
	 * @return true if can send an intent, false otherwise
	 */
	public static boolean tryToSendIntent(Context context, Intent sendIntent)
	{
		// Verify that the intent will resolve to an activity
		if (sendIntent.resolveActivity(context.getPackageManager()) != null)
		{
			context.startActivity(sendIntent);
			return true;
		}
		return false;
	}
}
