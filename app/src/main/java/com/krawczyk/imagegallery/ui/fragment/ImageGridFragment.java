package com.krawczyk.imagegallery.ui.fragment;

import android.app.Fragment;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.krawczyk.imagegallery.R;
import com.krawczyk.imagegallery.model.Items;
import com.krawczyk.imagegallery.ui.EmptyTextRecyclerView;
import com.krawczyk.imagegallery.ui.MediaUpdater;
import com.krawczyk.imagegallery.ui.adapter.ImageAdapter;

import java.util.List;

/**
 * A fragment that shows square images in the grid with given span.
 *
 * Created by Paweł Krawczyk on 14-01-2017.
 */
public class ImageGridFragment extends Fragment implements MediaUpdater {
    private static final String STATE_POSITION_INDEX = "state_position_index";
    private static final String SPAN_PORTRAIT = "span_portrait";
    private static final String SPAN_LANDSCAPE = "span_landscape";

    private ImageAdapter mImageAdapter;
    private List<Items> mCurrentImages;
    private GridLayoutManager mGridLayoutManager;
    private EmptyTextRecyclerView mRecyclerView;

    public static ImageGridFragment newInstance(int spanPortrait, int spanLandscape) {
        ImageGridFragment photoGrid = new ImageGridFragment();
        Bundle args = new Bundle();
        args.putInt(SPAN_PORTRAIT, spanPortrait);
        args.putInt(SPAN_LANDSCAPE, spanLandscape);
        photoGrid.setArguments(args);
        return photoGrid;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View result = inflater.inflate(R.layout.image_grid, container, false);
        Bundle args = getArguments();
        // Choose span mode for given orientation
        int spanCount;
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            spanCount = args.getInt(SPAN_PORTRAIT);
        } else {
            spanCount = args.getInt(SPAN_LANDSCAPE);
        }
        final int gridMargin = getResources().getDimensionPixelOffset(R.dimen.grid_margin);
        mRecyclerView = (EmptyTextRecyclerView) result.findViewById(R.id.image_grid);
        mGridLayoutManager = new GridLayoutManager(getActivity(), spanCount);
        mRecyclerView.setLayoutManager(mGridLayoutManager);

        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                       RecyclerView.State state) {
                outRect.set(gridMargin, gridMargin, gridMargin, gridMargin);
            }
        });

        mRecyclerView.setItemViewCacheSize(0);
        mImageAdapter = new ImageAdapter();
        mRecyclerView.setAdapter(mImageAdapter);

        if (mCurrentImages != null) {
            mImageAdapter.setMediaList(mCurrentImages);
        }
        if (savedInstanceState != null) {
            int index = savedInstanceState.getInt(STATE_POSITION_INDEX);
            mRecyclerView.scrollToPosition(index);
        }

        mRecyclerView.setEmptyView(result.findViewById(R.id.empty_view));
        return result;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mRecyclerView != null) {
            int index = mGridLayoutManager.findFirstVisibleItemPosition();
            outState.putInt(STATE_POSITION_INDEX, index);
        }
    }

    /**
     * Called when new media are available
     * @param medias List of new medias
     */
    @Override
    public void onMediaUpdated(List<Items> medias) {
        mCurrentImages = medias;
        if (mImageAdapter != null) {
            mImageAdapter.setMediaList(mCurrentImages);
        }
    }
}
