package com.krawczyk.imagegallery.dagger.component;

import com.krawczyk.imagegallery.api.FlickrFeedDownloader;
import com.krawczyk.imagegallery.dagger.module.AppModule;
import com.krawczyk.imagegallery.dagger.module.NetModule;
import com.krawczyk.imagegallery.ui.adapter.ImageAdapter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = { AppModule.class, NetModule.class })
public interface NetComponent
{
	void inject(FlickrFeedDownloader flickrImagesDownloader);
	void inject(ImageAdapter imageAdapter);
}
