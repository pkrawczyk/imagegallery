package com.krawczyk.imagegallery.api;

import android.util.Log;

import com.krawczyk.imagegallery.model.Feed;
import com.krawczyk.imagegallery.ui.MediaUpdater;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Query callback for flickr image download result. Handles success, error responses and the request
 * failure.
 *
 * Created by Paweł Krawczyk on 14-01-2017.
 */
public class FlickrQueryCallback implements Callback<Feed>
{
	private final static String TAG = FlickrQueryCallback.class.getName();
	private MediaUpdater mMediaUpdater;

	public FlickrQueryCallback(MediaUpdater mediaUpdater)
	{
		this.mMediaUpdater = mediaUpdater;
	}

	@Override
	public void onResponse(Call<Feed> call, Response<Feed> response)
	{
		if (response.isSuccessful())
		{
			Log.i(TAG, "Result: " + response.body());
			Feed feed = response.body();
			mMediaUpdater.onMediaUpdated(feed.getItems());
		}
		else
		{
			Log.w(TAG, "Result: " + response.errorBody());
		}
	}

	@Override
	public void onFailure(Call<Feed> call, Throwable t)
	{
		Log.e(TAG, "Failure result: ", t);
	}

}
