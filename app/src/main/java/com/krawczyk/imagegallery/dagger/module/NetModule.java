package com.krawczyk.imagegallery.dagger.module;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.krawczyk.imagegallery.api.ApiConstants;
import com.krawczyk.imagegallery.util.gson.GsonPConverterFactory;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;

@Module
public class NetModule
{
	String mBaseUrl = ApiConstants.FLICKR_IMAGES_BASE_URL;

	public NetModule()
	{
	}

	@Provides
	@Singleton
	Gson provideGson()
	{
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
		        .setLenient();
		return gsonBuilder.create();
	}

	@Provides
	@Singleton
	OkHttpClient provideOkHttpClient()
	{
		OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor()
		{
			@Override
			public Response intercept(Chain chain) throws IOException
			{
				Request.Builder ongoing = chain.request().newBuilder();
				ongoing.addHeader("Accept", "application/json;versions=1");
				return chain.proceed(ongoing.build());
			}
		}).build();
		return client;
	}

	@Provides
	@Singleton
	public Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient)
	{
		Retrofit retrofit = new Retrofit.Builder()
		        .addConverterFactory(new GsonPConverterFactory(gson)).baseUrl(mBaseUrl)
		        .client(okHttpClient).build();
		return retrofit;
	}
}
