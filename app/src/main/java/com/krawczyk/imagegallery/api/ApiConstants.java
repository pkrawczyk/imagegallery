package com.krawczyk.imagegallery.api;

/**
 * Provides api related constants
 *
 * Created by Paweł Krawczyk on 14-01-2017.
 */
public interface ApiConstants
{
	String FLICKR_IMAGES_BASE_URL = "https://api.flickr.com/services/feeds/";
	String PUBLIC_PHOTOS_URL_PART ="photos_public.gne?format=json";
}
