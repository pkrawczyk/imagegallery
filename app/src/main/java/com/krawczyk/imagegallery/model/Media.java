package com.krawczyk.imagegallery.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Represents media from Flickr API.
 * Implements {@link Parcelable} interface to be passed between activities with intents.
 *
 * AUTOMATICALLY generated file from json. See <a href="http://pojo.sodhanalibrary.com/">this
 * page</a> for details.
 * AUTOMATICALLY generated Parcable code using online tool. See <a href="http://www.parcelabler.com/">this
 * page</a> for details.
 */
public class Media implements Parcelable {

	@SerializedName("m")
	private String mUrl;

	public String getUrl()
	{
		return mUrl;
	}

	public void setUrl(String mUrl)
	{
		this.mUrl = mUrl;
	}

	@Override
	public String toString()
	{
		return "Media [mUrl = " + mUrl + "]";
	}

	protected Media(Parcel in) {
		mUrl = in.readString();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mUrl);
	}

	@SuppressWarnings("unused")
	public static final Parcelable.Creator<Media> CREATOR = new Parcelable.Creator<Media>() {
		@Override
		public Media createFromParcel(Parcel in) {
			return new Media(in);
		}

		@Override
		public Media[] newArray(int size) {
			return new Media[size];
		}
	};
}