package com.krawczyk.imagegallery.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

/**
 * RecyclerView with option to set an empty text view, if there is no data to show.
 *
 * Created by Paweł Krawczyk on 14-01-2017.
 */
public class EmptyTextRecyclerView extends RecyclerView
{
	private View mEmptyView;

	public EmptyTextRecyclerView(Context context)
	{
		super(context);
	}

	public EmptyTextRecyclerView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public EmptyTextRecyclerView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	/**
	 * Makes empty view visible (if available) if there are no items in RecyclerView and gone otherwise.
	 */
	private void checkIfEmpty()
	{
		if (mEmptyView != null && getAdapter() != null)
		{
			boolean isEmpty = getAdapter().getItemCount() == 0;
			mEmptyView.setVisibility(isEmpty ? VISIBLE : GONE);
			setVisibility(isEmpty ? GONE : VISIBLE);
		}
	}

	/**
	 * Observer for tracking changes in the RecyclerView's data.
	 */
	private final AdapterDataObserver mAdapterDataObserver = new AdapterDataObserver()
	{
		@Override
		public void onChanged()
		{
			super.onChanged();
			checkIfEmpty();
		}
		@Override
		public void onItemRangeInserted(int positionStart, int itemCount) {
			checkIfEmpty();
		}

		@Override
		public void onItemRangeRemoved(int positionStart, int itemCount) {
			checkIfEmpty();
		}
	};

	@Override
	public void setAdapter(Adapter adapter)
	{
		final Adapter oldAdapter = getAdapter();
		if (oldAdapter != null)
		{
			oldAdapter.unregisterAdapterDataObserver(mAdapterDataObserver);
		}
		super.setAdapter(adapter);
		if (adapter != null)
		{
			adapter.registerAdapterDataObserver(mAdapterDataObserver);
		}
	}

	/**
	 * Sets view to show when RecyclerView is empty.
	 * @param mEmptyView View to show.
     */
	public void setEmptyView(View mEmptyView)
	{
		this.mEmptyView = mEmptyView;
		checkIfEmpty();
	}
}