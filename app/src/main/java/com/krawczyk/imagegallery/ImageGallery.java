package com.krawczyk.imagegallery;

import android.app.Application;

import com.krawczyk.imagegallery.dagger.component.DaggerNetComponent;
import com.krawczyk.imagegallery.dagger.component.NetComponent;
import com.krawczyk.imagegallery.dagger.module.AppModule;
import com.krawczyk.imagegallery.dagger.module.NetModule;

/**
 * Application class. Provides dagger component for app and retrofit module.
 *
 * Created by Paweł Krawczyk on 14-01-2017.
 */
public class ImageGallery extends Application
{
	private static ImageGallery sInstance;
	private NetComponent mNetComponent;

	public static ImageGallery getInstance()
	{
		return sInstance;
	}

	@Override
	public void onCreate()
	{
		super.onCreate();
		sInstance = this;
		mNetComponent = DaggerNetComponent.builder().appModule(new AppModule(this))
		        .netModule(new NetModule()).build();
	}

	public NetComponent getNetComponent()
	{
		return mNetComponent;
	}
}
