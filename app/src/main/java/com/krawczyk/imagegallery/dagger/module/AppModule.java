package com.krawczyk.imagegallery.dagger.module;

import javax.inject.Singleton;

import android.app.Application;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule
{
	Application mApplication;

	public AppModule(Application mApplication)
	{
		this.mApplication = mApplication;
	}

	@Provides
	@Singleton
	Application provideApplication()
	{
		return mApplication;
	}
}
