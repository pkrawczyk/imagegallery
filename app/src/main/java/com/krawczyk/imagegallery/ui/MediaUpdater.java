package com.krawczyk.imagegallery.ui;

import com.krawczyk.imagegallery.model.Items;
import com.krawczyk.imagegallery.model.Media;

import java.util.List;

/**
 * An interface for an object that displays {@link Media} objects, when the list is updated.
 *
 * Created by Paweł Krawczyk on 14-01-2017.
 */
public interface MediaUpdater
{
	/**
	 * Called whenever new media are available.
	 *
	 * @param medias
	 *            The downloaded media.
	 */
	public void onMediaUpdated(List<Items> medias);
}
