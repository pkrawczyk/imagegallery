package com.krawczyk.imagegallery.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.krawczyk.imagegallery.ImageGallery;

/**
 * Utility class for checking network connectivity
 *
 * Created by Paweł Krawczyk on 14-01-2017.
 */
public class NetworkUtils {

    private NetworkUtils(){
        //Do not initialize
    }

    public static boolean isNetworkAvailable(){
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ImageGallery.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
