package com.krawczyk.imagegallery.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Represents items from Flickr API.
 * Implements implements Parcelable {@link Parcelable} interface to be passed between activities with intents.
 *
 * AUTOMATICALLY generated file from json. See <a href="http://pojo.sodhanalibrary.com/">this
 * page</a> for details.
 * AUTOMATICALLY generated Parcable code using online tool. See <a href="http://www.parcelabler.com/">this
 * page</a> for details.
 */
public class Items implements Parcelable
{
	private String tags;

	private String author;

	private String title;

	private String description;

	private String date_taken;

	private String link;

	private String author_id;

	private String published;

	private Media media;

	public String getTags()
	{
		return tags;
	}

	public void setTags(String tags)
	{
		this.tags = tags;
	}

	public String getAuthor()
	{
		return author;
	}

	public void setAuthor(String author)
	{
		this.author = author;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getDate_taken()
	{
		return date_taken;
	}

	public void setDate_taken(String date_taken)
	{
		this.date_taken = date_taken;
	}

	public String getLink()
	{
		return link;
	}

	public void setLink(String link)
	{
		this.link = link;
	}

	public String getAuthor_id()
	{
		return author_id;
	}

	public void setAuthor_id(String author_id)
	{
		this.author_id = author_id;
	}

	public String getPublished()
	{
		return published;
	}

	public void setPublished(String published)
	{
		this.published = published;
	}

	public Media getMedia()
	{
		return media;
	}

	public void setMedia(Media media)
	{
		this.media = media;
	}

	@Override
	public String toString()
	{
		return "Items [tags = " + tags + ", author = " + author + ", title = " + title
				+ ", description = " + description + ", date_taken = " + date_taken + ", link = "
				+ link + ", author_id = " + author_id + ", published = " + published + ", media = "
				+ media + "]";
	}

	protected Items(Parcel in) {
		tags = in.readString();
		author = in.readString();
		title = in.readString();
		description = in.readString();
		date_taken = in.readString();
		link = in.readString();
		author_id = in.readString();
		published = in.readString();
		media = (Media) in.readValue(Media.class.getClassLoader());
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(tags);
		dest.writeString(author);
		dest.writeString(title);
		dest.writeString(description);
		dest.writeString(date_taken);
		dest.writeString(link);
		dest.writeString(author_id);
		dest.writeString(published);
		dest.writeValue(media);
	}

	@SuppressWarnings("unused")
	public static final Parcelable.Creator<Items> CREATOR = new Parcelable.Creator<Items>() {
		@Override
		public Items createFromParcel(Parcel in) {
			return new Items(in);
		}

		@Override
		public Items[] newArray(int size) {
			return new Items[size];
		}
	};
}