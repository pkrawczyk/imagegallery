package com.krawczyk.imagegallery.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

/**
 * ViewHolder for recycling image views
 *
 * Created by Paweł Krawczyk on 14-01-2017.
 */
public class ImageViewHolder extends RecyclerView.ViewHolder {
        private final ImageView mImageView;

        public ImageViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView;
        }

    public ImageView getImageView() {
        return mImageView;
    }
}
