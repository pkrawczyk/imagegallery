package com.krawczyk.imagegallery;

/**
 * Keeps the application configurations constants.
 *
 * Created by Paweł Krawczyk on 14-01-2017.
 */
public interface AppConfigurations {

    // Number of images in one row of the grid for different orientations
    int PORTRAIT_SPAN = 3;
    int LANDSCAPE_SPAN = 5;
}
